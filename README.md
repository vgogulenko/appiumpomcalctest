## Appium tests for Android native calculator using Java+Maven+Junit+Page Object Model

## Purpose

Run your Appium Android tests "out-of-the-box" using Docker Android container with running Appium inside - you don't need to install anything except Docker+Maven+JDK on your Linux machine

---

## Requirements

Docker, Maven and JDK (>=8) are isntalled on your system. Works great on Ubuntu 14.04/16.04/18.04

For Mac OS users - please use Ubuntu VM with enabled nested virtualisation/hypervisor applications enabled in Parallels or VmWare Fusion. 


---

## How to run a test?

Execute just one command:

`docker run --privileged -d -p 6080:6080 -p 5554:5554 -p 5555:5555 -p 4723:4723 -e DEVICE="Nexus 5" -e APPIUM=true --name android-container butomo1989/docker-android-x86-8.1 && sleep 30 && docker exec android-container /bin/sh -c "adb emu kill" && mvn clean -Dtest=CalcTests test`

Will be downloaded Android Docker container with running Android Emulator+Appium server (you don't need to install Android SDK etc.) and Junit tests will be executed against. By default tests are running against "Nexus 5" API 27. To change configs please update corresponding capabilities in core.Driver.java. More information regarding Docker Android container you could find on https://github.com/butomo1989/docker-android

---

## Special Thanks

Thanks to butomo1989 user for great docker-android project and dockerized Android components. For more information see https://github.com/butomo1989/docker-android