package tests;

import core.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.CalculatorPageObject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class CalcTests {

    private AppiumDriver driver;
    private CalculatorPageObject pageObject;

    @Before()
    public void theCalculatorAppIsRunning() throws Throwable {
        driver = Driver.setupstart();
        pageObject = new CalculatorPageObject(driver);
    }

    @Test
    public void calcTest() {

        /* Positive case: verify '2 + 3 = 5' */
        pageObject.delButton().click();
        pageObject.digitButton(2).click();
        pageObject.sumButton().click();
        pageObject.digitButton(3).click();
        pageObject.equalButton().click();

        /* Verify that result is "5" */
        assertEquals("Incorrect Result", "5", pageObject.resultView().getText());
        //clear input field
        pageObject.clearButton().click(); //clear input field

        /* Positive case: verify '10 - 2 = 8' */
        pageObject.delButton().click();
        pageObject.digitButton(1).click();
        pageObject.digitButton(0).click();
        pageObject.subButton().click();
        pageObject.digitButton(2).click();
        pageObject.equalButton().click();

        /* Verify that result is "8" */
        assertEquals("Incorrect Result", "8", pageObject.resultView().getText());
        pageObject.clearButton().click();

        /* Negative case: verify '(10 - 2) * 2 != 20' */
        pageObject.advancedPad().click();
        pageObject.leftParenthesis().click();
        pageObject.decimalPointButton().click();
        pageObject.digitButton(1).click();
        pageObject.digitButton(0).click();
        pageObject.subButton().click();
        pageObject.digitButton(2).click();
        pageObject.advancedPad().click();
        pageObject.rightParenthesis().click();
        pageObject.decimalPointButton().click();
        pageObject.multButton().click();
        pageObject.digitButton(2).click();
        pageObject.advancedPad().click();
        pageObject.factorialButton().click();
        pageObject.decimalPointButton().click();
        pageObject.equalButton().click();

        /* verify that result is not equals 20 */
        assertNotEquals("Incorrect Result", "20", pageObject.resultView().getText());
        pageObject.clearButton().click();

        /* Positive case: verify 'sin(30) = 0.5' */
        pageObject.advancedPad().click();
        /* verify that RAD/DEG toggle switched to DEG */
        String text = "DEG";
        if (pageObject.radDegToggleButton().getText().equals(text)){
            pageObject.radDegToggleButton().click();
            assertEquals("Incorrect Result", "RAD", pageObject.radDegToggleButton().getText());
        }
        pageObject.sinButton().click();
        pageObject.decimalPointButton().click();
        pageObject.digitButton(3).click();
        pageObject.digitButton(0).click();
        pageObject.advancedPad().click();
        pageObject.rightParenthesis().click();
        pageObject.decimalPointButton().click();
        pageObject.equalButton().click();

        /* verify that sin(30 degrees) = 0.5 */
        assertEquals("Incorrect Result", "0.5", pageObject.resultView().getText());
        pageObject.clearButton().click();

        /* open history page and verify that our 4 formulas, which were executed above - are saved
        in history
         */
        pageObject.optionsButton().click();
        pageObject.historyButton().click();
        scrollToNeededElementByText("2+3");
        scrollToNeededElementByText("10−2");
        scrollToNeededElementByText("(10−2)×2!");
        scrollToNeededElementByText("sin(30)");
    }

    private void scrollToNeededElementByText(String text) {
        driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new " +
                "UiSelector().scrollable(true).instance(0))" +
                ".scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0));"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
