package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CalculatorPageObject {

    private AppiumDriver driver;
    private String appPkg = "com.android.calculator2:id/";

    private By del = By.id(appPkg + "del");
    private By clr = By.id(appPkg + "clr");
    private By add = By.id(appPkg + "op_add");
    private By sub = By.id(appPkg + "op_sub");
    private By mult = By.id(appPkg + "op_mul");
    private By div = By.id(appPkg + "op_div");
    private By equal = By.id(appPkg + "eq");
    private By pad_advanced = By.id(appPkg + "pad_advanced");
    private By lparen = By.id(appPkg + "lparen");
    private By rparen = By.id(appPkg + "rparen");
    private By dec_point = By.id(appPkg + "dec_point");
    private By op_fact = By.id(appPkg + "op_fact");
    private By sin = By.id(appPkg + "fun_sin");
    private By history = By.xpath("//android.widget.TextView[@text='History']");
    private By result = By.id(appPkg + "result");
    private By toggle = By.id(appPkg + "toggle_mode");
    private By options = By.className("android.widget.ImageButton");


    public CalculatorPageObject(AppiumDriver driver) {
        this.driver = driver;
    }

    public WebElement delButton(){
        return driver.findElement(del);
    }

    public WebElement clearButton(){
        return driver.findElement(clr);
    }

    public WebElement digitButton(int number) {
        return driver.findElement(By.id(appPkg + "digit_" + number));
    }

    public WebElement sumButton() {
        return driver.findElement(add);
    }

    public WebElement subButton() {
        return driver.findElement(sub);
    }

    public WebElement multButton() {
        return driver.findElement(mult);
    }

    public WebElement divButton() {
        return driver.findElement(div);
    }

    public WebElement advancedPad() {
        return driver.findElement(pad_advanced);
    }

    public WebElement leftParenthesis() {
        return driver.findElement(lparen);
    }

    public WebElement rightParenthesis() {
        return driver.findElement(rparen);
    }

    public WebElement decimalPointButton() {
        return driver.findElement(dec_point);
    }

    public WebElement factorialButton() {
        return driver.findElement(op_fact);
    }

    public WebElement sinButton() {
        return driver.findElement(sin);
    }
    public WebElement equalButton() {
        return driver.findElement(equal);
    }

    public WebElement historyButton(){
        return driver.findElement(history);
    }
    public WebElement resultView(){
        return driver.findElement(result);
    }
    public WebElement radDegToggleButton(){
        return driver.findElement(toggle);
    }
    public WebElement optionsButton(){
        return driver.findElement(options);
    }
}