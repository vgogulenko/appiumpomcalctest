package core;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    private static AppiumDriver driver = null;

    private Driver() {
    }

    public static AppiumDriver setupstart() throws MalformedURLException {
        if (driver == null) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "AndroidTestDevice");
            capabilities.setCapability("avd", "nexus_5_8.1");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("avdLaunchTimeout", 300000);
            capabilities.setCapability("avdReadyTimeout", 300000);
            capabilities.setCapability("appPackage", "com.android.calculator2");
            capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
            capabilities.setCapability("appWaitActivity", "com.android.calculator2.Calculator");
            driver = new AppiumDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        }
        return driver;
    }
}
